import Solution from "./Solution.js";

export default class Day1 extends Solution {
  async prepareData(input: string) {
    return input.split("\n").map((line) => parseInt(line));
  }

  async solveSilver(data: Awaited<ReturnType<Day1["prepareData"]>>) {
    return data.map((num) => Math.trunc(num / 3) - 2).reduce((a, b) => a + b);
  }

  async solveGold(data: Awaited<ReturnType<Day1["prepareData"]>>) {
    return data
      .map((num) => {
        let fuel = 0;
        let current = num;
        while (current > 0) {
          current = Math.trunc(current / 3) - 2;
          if (current > 0) {
            fuel += current;
          }
        }

        Math.trunc(num / 3) - 2;
        return fuel;
      })
      .reduce((a, b) => a + b);
  }
}
